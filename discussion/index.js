//[SECTION] Data Modelling

//1. Identify what information we want to gather from the customers in order to determine wether the user's identity is true.


//=>WHY? 
	//1 TO PROPERLY PLAN out what information will be deemed useful.
	//2. to lessen the chances or scenarios of having to modify or edit the data stored in the database.
	//3. To anticipate how this data/information would relate to each other.

//2. Create an ERD to represetnt the enetitites inside the database as well as to descrive the relationships amongst them.

		//users can have multiple subjects
		//subjects can have multiple enrolees

		//user can have multiple transactions
		//transaction can only belong to a single user.

		//a single transaction can have multiple courses
		//a course can be part of multiple transactions.

//3. Convert and Translate the ERD into a JSON-like syntax in order to describe the structure of a document inside the collection by creating a MOCK data. 

		//NOTE: Creating/using mock data can play an integral role during the testing phase of any app development.

		//you want to be able to utilize the storage in your database


//TASK: Create a Course Booking System for an Institution.

	//What are the minimum information would I need to collect from the customers/students.
	//the following information described below would identify the structure of the user in our app
	// 1. first Name
	// 2. last Name
	// 3. Middle Name
	// 4. email address
	// 5. PIN/password
	// 6. mobile number
	// 7. birthdate
	// 8. sex
	// 9. isAdmin: => role and restrictions/limitations that this user would have in our app.
	//10. dateTimeRegistered => when the student signed up/enrolled in the institution.

	//Course/Subject
		//1. name/title
		//2. course code
		//3. course decription
		//4. course units
		//5. course instructor
		//6. isActive => to describe if the course is being offered by the institution.
		//7. dateTimeCreated => for us to identify when the course was added to the database.
		//8. number of available slots


	//aS A FULL STACK WEB DEVELOPER (FROMT, BACKEND, DATABSE)
	//The more informaation you gather the more difficult it is to scale and manage the collection. it is going to be more difficult to maintain.
